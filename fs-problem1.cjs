/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs=require('fs');
const path=require('path');
function createAndDeleteJSONfile(folder,numberOfFiles)
{
    fs.mkdir(folder,(error)=>{
        if(error)
        {
            console.log(error)
        }
        else{
            console.log('New folder created')
            if(numberOfFiles===0)
            {
                deleteFolder(folder)
            }
            else{
                gererateRandomFile(folder,numberOfFiles)
            }
        }
    })
    function gererateRandomFile(folder,numberOfFiles)
    {
        const cb=function callback(error,value){
            if(error){
                console.log(err)
            }
            else{
                //console.log('file created')
                deleteFiles(value,folder)
            }

        }
        let numberOfSuccessExicution=0;
        const arrayOfFileNames=[];
        for(let index=0;index<numberOfFiles;index++)
        {
            const nameOfFile=`${index+1}.json`
            fs.writeFile(`${folder}/${nameOfFile}`,'',(error,value)=>{
                if(error)
                {
                    console.log(error)
                }
                else{
                    numberOfSuccessExicution+=1
                    console.log(`${nameOfFile} created`)
                    arrayOfFileNames.push(nameOfFile);                
                }
                if(numberOfSuccessExicution===numberOfFiles)
                {
                    cb(error,arrayOfFileNames);
                }
            })
        }
    }

    function deleteFiles(arrayOfFileNames,folder)
    {
        const cb=function callback(error,value){
            if(error)
            {
                console.log(error)
            }
            else{
                console.log(value)
                deleteFolder(folder)
            }
        }
        let count=0;
        for(let index=0;index<arrayOfFileNames.length;index++)
        {
            fs.unlink(`${folder}/${arrayOfFileNames[index]}`,(error)=>{
                count++

                if(count===arrayOfFileNames.length)
                {
                    cb(error,`${`All files deleted`}`)
                }
                else{
                    console.log(`file deleted`)
                }
            })
        }

    }
    function deleteFolder(folder)
    {
        fs.rmdir(`${folder}`,(error)=>{
            if(error)
            {
                console.log(error)
            }
            else{
                console.log('Folder deleted')
            }
        })
    }
}
module.exports=createAndDeleteJSONfile;
