/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs=require('fs');
const path=require('path')
const lipusm=path.join(__dirname,'./lipsum.txt')
const problem2=(lipsum)=>{

//1. Read the given file lipsum.txt

fs.readFile(lipusm,'utf-8',function callBack(err,data){
    if(err)
    {
        console.log(err)
    }
    //console.log(data)

    //2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

    const dataUpperCase = data.toUpperCase();
    
    fs.writeFile('filenames.txt',dataUpperCase,function err(err){
        if(err){
            console.log(err)
        }
        //console.log('file created')

        //3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

        fs.readFile('filenames.txt','utf-8',function error(erro,text){
            //console.log(text)
            if(erro){
                console.log(erro)
            }
            const dataLowerCase=text.toLowerCase();
            const splitIntoSentences=dataLowerCase.split('. ')
            const joinSpilData=splitIntoSentences.join('.\n')
            //console.log(joinSpilData);
            fs.writeFile('newFile.txt',joinSpilData,function isErr(isEr){
                if(isEr){
                    console.log(isEr)
                }
                fs.writeFile('filenames.txt','newFile.txt',function isEr(err){
                    if(err){
                        console.log(err)
                    }

                    //4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                    fs.readFile('newFile.txt','utf-8',function err(error,data){
                        if(error)
                        {
                            console.log(error)
                        }
                        //console.log(data)
                        const sortedData = data.split('\n').sort().join('\n')
                        fs.writeFile('sortedData.txt',sortedData, function err(error){
                            if(error)
                            {
                                console.log(error)
                            }
                            fs.appendFile('filenames.txt',' sortedData.txt',function err(error){
                                if(error){
                                    console.log(error);
                                }

                                //5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

                                fs.readFile('filenames.txt','utf-8',function error(err,data){
                                    if(err)
                                    {
                                        console.log(err)
                                    }
                                    const filesToBeDelete=data.split(' ');
                                    //console.log(filesToBeDelete)
                                    filesToBeDelete.map((fileName)=>{
                                        fs.unlink(fileName, function err(error){
                                            if(error)
                                            {
                                                console.log(error)
                                            }
                                            console.log(`File ${fileName} deleted successfully`)
                                        })
                                    })
                                })
                            })
                        })
                    })

                })
            })
        })
    })
})
}

module.exports=problem2;
